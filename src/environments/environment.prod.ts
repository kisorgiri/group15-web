export const environment = {
  production: true,
  BaseUrl: 'http://localhost:9090/api/',
  ImgUrl: 'http://localhost:9090/file/images/',
  SocketUrl: 'http://localhost:9091'
};
