import { Component, OnInit } from '@angular/core';
import { ItemService } from '../services/item.service';
import { Router } from '@angular/router';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { Item } from 'src/app/shared/models/item.models';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.css']
})
export class SearchItemComponent implements OnInit {
  submitting: boolean = false;
  item;
  results = [];
  allItems = [];
  categories = [];
  names = [];
  showName: boolean = false;
  constructor(
    public itemService: ItemService,
    public router: Router,
    public notifyService: NotifyService
  ) {
    this.item = new Item({});
    this.itemService.fetch()
      .subscribe(
        (data: any) => {
          this.allItems = data;
          this.allItems.forEach((item, i) => {
            if (this.categories.indexOf(item.category) === -1) {
              this.categories.push(item.category);
            }
          });
        }, error => {
          console.log('error in fetching list >>', error)
        })
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.itemService.search(this.item)
      .subscribe((data: any) => {
        this.submitting = false;
        console.log('data >>', data);
        if (data.length) {
          this.results = data;
        } else {
          this.notifyService.showInfo('No any Item matched your search query');
        }
      }, error => {
        this.submitting = false;
        this.notifyService.showError(error);
      })
  }

  resetSearch() {
    console.log('reset called');
    this.results.length = 0;
    this.item = new Item({});
  }

  categorySelected(category) {
    console.log('categoty >>', category);
    this.showName = true;
    this.names = this.allItems.filter((data, i) => {
      if (data.category === category) {
        return true;
      }
    })
  }

}
