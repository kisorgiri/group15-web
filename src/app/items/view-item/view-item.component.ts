import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemService } from '../services/item.service';
import { Router } from '@angular/router';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { Item } from 'src/app/shared/models/item.models';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {
  items: Array<Item>;
  loading: boolean = false;
  imgUrl;
  @Input() inputData: any;
  @Output() searchagain = new EventEmitter();

  constructor(
    public itemService: ItemService,
    public router: Router,
    public notifyService: NotifyService
  ) {
    this.imgUrl = environment.ImgUrl;
    // constructor will get executed first
    //input data are not visible here
  }

  ngOnInit() {
    console.log('this.inputData', this.inputData);
    // life cycle hook of angular component
    // on init 
    // on change
    // on destroy
    // it will folllow constructor
    if (this.inputData) {
      this.items = this.inputData;
    } else {
      this.loading = true;
      this.itemService.fetch()
        .subscribe(
          (data: any) => {
            this.items = data;
            this.loading = false;
            console.log('items .>>', data);
          },
          error => {
            this.loading = false;
            this.notifyService.showError(error);
          });
    }

  }
  removeItem(id, index) {
    let confirmation = confirm('Are you sure to remove?');
    if (confirmation) {
      this.itemService.remove(id).subscribe(data => {
        this.notifyService.showInfo('item deleted');
        this.items.splice(index, 1)
      }, error => {
        this.notifyService.showError(error);
      })
    }
  }
  searchAgain() {
    //you shoud communicate with search component
    this.searchagain.emit('searchAgain');
  }

}
