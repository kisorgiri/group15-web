import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/shared/models/item.models';
import { Router } from '@angular/router';
import { ItemService } from '../services/item.service';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { UploadService } from 'src/app/shared/services/upload.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  item;
  submitting: boolean = false;
  uploadArray = [];
  url;
  constructor(
    public router: Router,
    public itemService: ItemService,
    public uploadService: UploadService,
    public notify: NotifyService
  ) {
    this.item = new Item({});
    this.url = environment.BaseUrl + '/item?token=' + localStorage.getItem('token');
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;

    this.uploadService.upload(this.item, this.uploadArray, 'POST', this.url)
      .subscribe((data) => {
        this.notify.showSuccess('Item Added Successfully');
        this.router.navigate(['/item/view']);
      }, error => {
        this.notify.showError(error);
        this.submitting = false;
      })

  }
  fileChanged(ev) {
    // if single upload
    this.uploadArray = ev.target.files;
    // if multiple
    // this.uploadArray.push(ev.target.files[0]);

  }

}
