import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemService } from '../services/item.service';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {
  item;
  id;
  submitting: boolean = false;
  loading: boolean = false;
  imgUrl;
  url;
  uploadArr = [];
  constructor(
    public router: Router,
    public activeRouter: ActivatedRoute,
    public itemService: ItemService,
    public notifyService: NotifyService
  ) {
    this.id = this.activeRouter.snapshot.params['id'];
    this.imgUrl = environment.ImgUrl;
    this.url = environment.BaseUrl + 'item/' + this.id + '?token=' + localStorage.getItem('token');
  }

  ngOnInit() {
    this.loading = true;
    this.itemService.fetchById(this.id)
      .subscribe(
        data => {
          this.loading = false;
          this.item = data;
          console.log(this.item);
        },
        error => {
          this.loading = false;
          this.notifyService.showError(error);
        })
  }

  submit() {
    this.submitting = true;
    this.itemService.upload(this.item, this.uploadArr, 'PUT')
      .subscribe(
        data => {
          this.router.navigate(['/item/view']);
          this.notifyService.showInfo('Item update successfull');
        },
        error => {
          this.notifyService.showError(error);
          this.submitting = false;
        })
  }

  fileChanged(ev) {
    this.uploadArr = ev.target.files;
  }

}
