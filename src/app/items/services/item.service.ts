import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Item } from 'src/app/shared/models/item.models';
import { BaseService } from 'src/app/shared/services/base.service';
import { Observable } from 'rxjs';


@Injectable()
export class ItemService extends BaseService {
    constructor(
        public http: HttpClient
    ) {
        super('item');
    }

    add(data: Item) {
        return this.http.post(this.url, data, this.getOptionsWithToken())
    }

    update(data: Item) {
        return this.http.put(this.url + data._id, data, this.getOptionsWithToken());
    }

    remove(id: string) {
        return this.http.delete(this.url + id, this.getOptionsWithToken());
    }
    fetch() {
        return this.http.get(this.url, this.getOptionsWithToken());
    }
    fetchById(id: string) {
        return this.http.get(this.url + id, this.getOptionsWithToken());

    }
    search(condition: Item) {
        return this.http.post(this.url + 'search', condition, this.getOptionsWithToken());
    }

    upload(item: Item, files: Array<any>, method) {
        return Observable.create((observer) => {
            const xhr = new XMLHttpRequest();
            const formData = new FormData();
            if (files.length) {
                formData.append('file', files[0], files[0].name);
            }
            for (let key in item) {
                formData.append(key, item[key]);
            }
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let endUrl = this.url + '?token=' + localStorage.getItem('token')
            if (method == 'PUT') {
                endUrl = this.url + '/' + item._id + '?token=' + localStorage.getItem('token')
            }
            xhr.open(method, endUrl, true);
            xhr.send(formData);
        })

    }
}