import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  paramValue;
  user;
  submitting: boolean = false;
  constructor(
    public router: Router,
    public authService: AuthService,
    public notifyService: NotifyService,
    public activeRoute: ActivatedRoute
  ) {
    this.paramValue = this.activeRoute.snapshot.params['token'];
    this.user = new User({});
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.user.token = this.paramValue;
    this.authService.resetPassword(this.user)
      .subscribe((data) => {
        this.notifyService.showInfo('Password reset successfull, Please login to continue');
        this.router.navigate(['/auth/login']);
      }, err => {
        this.notifyService.showError(err);
        this.submitting = false;
      });
  }

}
