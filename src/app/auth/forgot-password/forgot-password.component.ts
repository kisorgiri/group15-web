import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  constructor(
    public router: Router,
    public authService: AuthService,
    public notifyService: NotifyService
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }
  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.user)
      .subscribe((data) => {
        this.notifyService.showInfo('Password reset link sent to your email please check your inbox');
        this.router.navigate(['/auth/login']);
      }, err => {
        this.submitting = false;
        this.notifyService.showError(err);
      })
  }

}
