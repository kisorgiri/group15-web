import { Component, OnInit } from '@angular/core';
import { SocketService } from 'src/app/shared/services/socket.service';
import { NotifyService } from 'src/app/shared/services/notify.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  msgBody: any;
  messages = [];
  users = [];
  currentUser;
  isTyping: boolean = false;
  constructor(
    public socketService: SocketService,
    public notifyService: NotifyService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.socketService.socket.emit('new-user', this.currentUser.username);
    this.msgBody = {
      msg: '',
      senderName: '',
      senderId: '',
      receiverId: '',
      time: new Date()
    }
  }

  ngOnInit() {
    this.runSocket();
  }

  runSocket() {
    this.socketService.socket.on('reply-msg', (data) => {
      console.log('msg from server >>', data);

      this.messages.push(data);
    });
    this.socketService.socket.on('reply-msg-user', (data) => {
      console.log('msg from server >>', data);
      this.msgBody.receiverId = data.senderId;
      this.messages.push(data);
    });

    this.socketService.socket.on('users', (data) => {
      this.users = data;
    })

    // every events that are emitted from server will lies here
  }

  submit() {
    this.users.forEach((item, i) => {
      if (item.name == this.currentUser.username) {
        this.msgBody.senderId = item.id;
      }
    })
    if (!this.msgBody.receiverId) {
      return this.notifyService.showInfo('Please select a user to continue');
    }
    this.msgBody.senderName = this.currentUser.username;
    console.log('here >>', this.msgBody);
    this.socketService.socket.emit('new-msg', this.msgBody);
    this.msgBody.msg = '';
  }

  selectUser(receiverId) {
    console.log('receiver id>>', receiverId);
    this.msgBody.receiverId = receiverId
  }

  focusedIn(bool) {
    console.log('bool >>', bool);
  }
}
