import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NotifyService } from './shared/services/notify.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'group15-web';

  constructor(public router: Router,
    public notifyService: NotifyService) {
    this.router.events.subscribe((nav: NavigationEnd) => {
      if (nav.url) {
        var endPoint = nav.url.split('/')[1];
        if (endPoint && endPoint != 'auth') {
          if (!localStorage.getItem('token')) {
            this.notifyService.showInfo('session timeout please login again');
            this.router.navigate(['/auth/login']);
          }
        }
      }
    })
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  isLoggedIn() {
    return localStorage.getItem('token') ? true : false;
  }

}
