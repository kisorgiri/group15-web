import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';


@Injectable()
export class UploadService {
    constructor() {
    }

    upload(data: any, files: any, method: string, url: string) {
        return Observable.create((observer) => {
            const xhr = new XMLHttpRequest();
            const formData = new FormData();
            if (files.length) {
                formData.append('file', files[0], files[0].name);
            }
            for (let key in data) {
                formData.append(key, data[key]);
            }
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            // let endUrl = url + '?token=' + localStorage.getItem('token')
            // if (method == 'PUT') {
            //     endUrl = url + '/' + item._id + '?token=' + localStorage.getItem('token')
            // }
            xhr.open(method, url, true);
            xhr.send(formData);
        });
    }
}